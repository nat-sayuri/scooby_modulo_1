# Copyright 2020 Giovani Bernardes
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch Gazebo with a world that has Scooby, as well as the follow node."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')
    pkg_scooby_gazebo = get_package_share_directory('scooby_gazebo')
    
    use_teleop = LaunchConfiguration('use_teleop')

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py'),
        )
    )

    #Follow node
    #follow = Node(
    #    package='scooby_follow',
    #    node_executable='scooby_follow',
    #    output='screen',
    #    remappings=[
    #        ('cmd_vel', '/scooby/cmd_vel'),
    #        ('laser_scan', '/scooby/laser_scan')
    #    ]
    #)

    start_scooby_teleop_cmd = Node(
        condition=IfCondition(use_teleop),
        package='scooby_teleop',
        executable='scooby_teleop',
        name='scooby_teleop',
        output='screen',
        parameters=[{'use_sim_time': True}],
        )

    # RViz
    rviz = Node(
        package='rviz2',
        executable='rviz2',
        arguments=['-d', os.path.join(pkg_scooby_gazebo, 'rviz', 'scooby_gazebo.rviz')],
        condition=IfCondition(LaunchConfiguration('rviz'))
    )

    return LaunchDescription([
        DeclareLaunchArgument(
          'world',
          default_value=[os.path.join(pkg_scooby_gazebo, 'worlds', 'scooby_factory_v2.world'), ''],
          description='SDF world file'),
        DeclareLaunchArgument('use_teleop', default_value='true',
                              description='Open scooby_teleop'),
        DeclareLaunchArgument('rviz', default_value='true',
                              description='Open RViz.'),
        gazebo,
        start_scooby_teleop_cmd,
        rviz
    ])
