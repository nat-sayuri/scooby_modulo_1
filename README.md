# Scooby the Sansung robot


Packages for launching Scooby demo, which uses Gazebo and ROS 2.

![Scooby Empty](scooby.gif)

Dolly has been tested on:

* ROS 2 version:
    * ROS Foxy: `foxy` branch
* Gazebo version:
    * Gazebo 11
* Operating system:
    * Ubuntu Focal (20.04)

## Install

Install instructions for Ubuntu Focal.

1. Install the appropriate ROS 2 version as instructed [here](https://index.ros.org/doc/ros2/Installation/Linux-Install-Debians/).

1. Install `gazebo_ros_pkgs`, which also installs Gazebo. Substitute `<distro>` with `foxy` :

        sudo apt install ros-<distro>-gazebo-ros-pkgs

1. Clone Scooby:

        mkdir -p ~/ws/src
        cd ~/ws/src
        git clone https://github.com/...

1. Build and install:

        cd ~/ws
        colcon build

## Run

1. Setup environment variables (the order is important):

        . /usr/share/gazebo/setup.sh
        . ~/ws/install/setup.bash

1. Launch Scooby in a Sansung plant 01:

        ros2 launch scooby_gazebo scooby.launch.py world:=scooby_SGBR01.world

1. Launch Dolly in an empty plant:

        ros2 launch scooby_gazebo scoobt.launch.py world:=scooby_empty.world

## Packages

This repository contains 2 packages:

* `scooby`: Metapackage which provides all other packages.
* `scooby_follow`: Provides node with follow logic.
* `scooby_gazebo`: Sansung Robot model, simulation world and launch scripts.

# TODO

* Make Scooby's model available to RViz

